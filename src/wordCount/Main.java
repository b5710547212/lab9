package wordCount;

import java.net.MalformedURLException;
import java.net.URL;

public class Main {

	public static void main(String[] args) throws MalformedURLException
	{
		StopWatch sw = new StopWatch();
		sw.start();
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL( DICT_URL );
		WordCounter counter = new WordCounter();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount( );
		sw.stop();
		System.out.println("Reading words from:"+DICT_URL);
		System.out.println("Counted " + syllables + " syllables in " + wordcount + " words");
		System.out.printf("Elapsed time: %.3f secs",sw.getElapsed());
	}
	
}
